FROM registry.git.autistici.org/ai3/docker/apache2-php-base:master

COPY noblogs /opt/noblogs/www
COPY wp-config.php /opt/noblogs/www/wp-config.php
COPY db-config.php /opt/noblogs/www/db-config.php
COPY db-backends.php /opt/noblogs/www/r2db/db-backends.php
COPY conf /tmp/conf
COPY build.sh /tmp/build.sh
COPY post-upgrade.sh /post-upgrade.sh
RUN /tmp/build.sh && rm /tmp/build.sh

