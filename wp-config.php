<?php

/**
 * First of all, read our own configuration file.
 *
 * Store the result in an associative array '$noblogs_config'.
 */
$noblogs_config = json_decode(file_get_contents('/etc/noblogs/config.json'), true);

/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */
define('BP_ENABLE_USERNAME_COMPATIBILITY_MODE',true);
define('FORCE_SSL_ADMIN', true);
define('FORCE_SSL_LOGIN', true);
if ( !defined('WP_CACHE') ) {
	define('WP_CACHE', true);
}
if ( ! defined('WPCACHEHOME') ){
   define( 'WPCACHEHOME', dirname(__FILE__) . '/wp-content/plugins/wp-super-cache/');
}
define('WP_ALLOW_MULTISITE', true);
define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', true);
$base = '/';
define('DOMAIN_CURRENT_SITE', 'noblogs.org');
define('PATH_CURRENT_SITE', '/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);
define('DISALLOW_FILE_MODS', true);
//define('DISALLOW_FILE_EDIT', false);
define('DISABLE_WP_CRON', true);
//AI patch: set to true if you want to activate query profiling
define('AI_DB_PROFILER', false);
//disable automatic updates
define( 'AUTOMATIC_UPDATER_DISABLED', true );

// Use Apache's mod_xsendfile for static content.
define('WPMU_SENDFILE', true);

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'noblogs');

/** MySQL database username */
define('DB_USER', 'noblogsdb');

/** MySQL database password */
define('DB_PASSWORD', 'non e questa');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/** SMTP settings. Check wp-includes/class-phpmailer.php for possible values for some fields. */
define('SMTP_HOST', 'mail-frontend.investici.org');
define('SMTP_PORT', '10025');
define('SMTP_SECURE', '');
define('SMTP_AUTH', false);
define('SMTP_USER', '');
define('SMTP_PASS', '');
define('SMTP_FROM', 'noreply@autistici.org');
define('SMTP_NAME', 'NoBlogs');
define('SMTP_DEBUG', 0);

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         $noblogs_config['secrets']['auth_key']);
define('SECURE_AUTH_KEY',  $noblogs_config['secrets']['secure_auth_key']);
define('LOGGED_IN_KEY',    $noblogs_config['secrets']['logged_in_key']);
define('NONCE_KEY',        $noblogs_config['secrets']['nonce_key']);
define('AUTH_SALT',        $noblogs_config['secrets']['auth_salt']);
define('SECURE_AUTH_SALT', $noblogs_config['secrets']['secure_auth_salt']);
define('LOGGED_IN_SALT',   $noblogs_config['secrets']['logged_in_salt']);
define('NONCE_SALT',       $noblogs_config['secrets']['nonce_salt']);

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress.  A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de.mo to wp-content/languages and set WPLANG to 'de' to enable German
 * language support.
 */
define ('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */

/*
 * A/I Patch: get more replicas of the partitions in the flexihash calculations.
 */
define('R2DB_FLEXIHASH_REPLICAS', 128);

/*
 * A/I patch: global recaptcha key.
 */
if (array_key_exists('recaptcha', $noblogs_config)) {
    define('GLOBAL_RECAPTCHA_KEY', $noblogs_config['recaptcha']['key']);
    define('GLOBAL_RECAPTCHA_PRIVATE_KEY', $noblogs_config['recaptcha']['private_key']);
}

/*
 * A/I Patch: print debug info on demand when the debug cookie is set.
 */
if (array_key_exists('debug_cookie_name', $noblogs_config) &&
    !empty($_COOKIE[$noblogs_config['debug_cookie_name']])) {
        define('WP_DEBUG', true);
        define('WP_DEBUG_LOG', true);
        define('WP_DEBUG_DISPLAY', false);
        define('AI_LOG_HYPERDB',true);
} else {
    define('WP_DEBUG', false);
    define('AI_LOG_HYPERDB',false);
}

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') ) {
	define('ABSPATH', dirname(__FILE__) . '/');
}

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
