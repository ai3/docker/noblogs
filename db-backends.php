<?php

function noblogs_load_backends($hashptr) {
  global $wpdb;
  global $noblogs_config;
  $backend_map = array();
  foreach ($noblogs_config['db_config']['backends'] as $backend_name => $backend) {
    $b = array(
       "host" => $backend['host'] . ":" . $backend['port'],
       "user" => $backend['user'],
       "password" => $backend['password'],
       "name" => $backend['name'],
       "dataset" => $backend_name,
       "read" => 1,
       "write" => 1,
       "timeout" => 10
    );
    $wpdb->add_database($b);
    $hashptr->addTarget($backend_name);
    $backend_map[$backend_name] = $b;
  }
  return $backend_map;
}

function noblogs_load_global_dataset() {
  global $wpdb;
  global $noblogs_config;
  $master = $noblogs_config['db_config']['master'];
  if ($noblogs_config['db_config']['is_master']) {
    $wpdb->add_database(array(
       "host" => "127.0.0.1:" . $master['port'],
       "user" => $master['user'],
       "password" => $master['password'],
       "name" => $master['name'],
       "dataset" => "global",
       "read" => 1,
       "write" => 1,
       "timeout" => 2
    ));
  } else {
    $wpdb->add_database(array(
       "host" => $master['host'] . ":" . $master['port'],
       "user" => $master['user'],
       "password" => $master['password'],
       "name" => $master['name'],
       "dataset" => "global",
       "read" => 0,
       "write" => 1,
       "timeout" => 2
    ));
    $wpdb->add_database(array(
       "host" => "127.0.0.1:" . $master['port'],
       "user" => $master['user'],
       "password" => $master['password'],
       "name" => $master['name'],
       "dataset" => "global",
       "read" => 1,
       "write" => 0,
       "timeout" => 2
    ));
  }
}
