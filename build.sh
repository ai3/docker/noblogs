#!/bin/sh
#
# Install script for git.autistici.org/ai/website
# inside a Docker container.
#
# The installation procedure requires installing some
# dedicated packages, so we have split it out to a script
# for legibility.

# Packages that are only used to build the site. These will be
# removed once we're done.
BUILD_PACKAGES="curl rsync"

# Packages required to serve the website and run the services.
# We have to keep the python3 packages around in order to run
# chaperone (installed via pip).
PACKAGES="
        libapache2-mod-xsendfile
        libapache2-mod-security2
        modsecurity-crs
        php-xml
        php-mysql
        php-memcached
        php-mbstring

        noblogs-cli
"

# Additional Apache modules to enable.
APACHE_MODULES_ENABLE="
        rewrite
        security2
        xsendfile
"

# Additional config snippets to enable for Apache.
APACHE_CONFIG_ENABLE="
        modsecurity-custom
"

# Sites to enable.
APACHE_SITES="
        noblogs.org
        noblogs.ai-cdn.net
"

# The default bitnami/minideb image defines an 'install_packages'
# command which is just a convenient helper. Define our own in
# case we are using some other Debian image.
if [ "x$(which install_packages)" = "x" ]; then
    install_packages() {
        env DEBIAN_FRONTEND=noninteractive apt-get install -qqy --no-install-recommends "$@"
    }
fi

set -e

apt-get -q update
install_packages ${BUILD_PACKAGES} ${PACKAGES}

# Install the configuration, overlayed over /etc.
rsync -a /tmp/conf/ /etc/

# Setup apache.
a2enmod -q ${APACHE_MODULES_ENABLE}
a2enconf -q ${APACHE_CONFIG_ENABLE}
a2ensite ${APACHE_SITES}

# Set up modsecurity.
# The file is named 00modsecurity.conf so it is loaded first.
mv /etc/modsecurity/modsecurity.conf-recommended /etc/modsecurity/00modsecurity.conf

# This needs to be writable for mod security to be able to start.
install -d -m 1777 /var/log/apache2

# Ensure that the mount points exist.
mkdir -p /opt/noblogs/www/wp-content/blogs.dir
mkdir -p /opt/noblogs/www/wp-content/cache
mkdir -p /var/lib/php/uploads

# Install wp-cli directly from the net, along with minimal
# configuration, passed via a wrapper.
curl -o /usr/bin/wp-cli.phar https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
cat > /etc/wp-cli.yml <<EOF
path: /opt/noblogs/www
color: false
user: admin
disabled_commands:
  - db drop
  - plugin install
EOF
chmod 0644 /etc/wp-cli.yml
cat > /usr/bin/wp <<EOF
#!/bin/sh
export WP_CLI_CONFIG_PATH=/etc/wp-cli.yml
export WP_CLI_CUSTOM_SHELL=/bin/sh
export WP_CLI_DISABLE_AUTO_CHECK_UPDATE=1
exec /usr/bin/php /usr/bin/wp-cli.phar "\$@"
EOF
chmod 0755 /usr/bin/wp /usr/bin/wp-cli.phar

# Remove packages used for installation.
apt-get remove -y --purge ${BUILD_PACKAGES}
apt-get autoremove -y
apt-get clean
rm -fr /var/lib/apt/lists/*
rm -fr /tmp/conf

